'use strict';
module.exports = (sequelize, DataTypes) => {
  const registroprof = sequelize.define('registroprof', {
    IdUser: DataTypes.STRING,
    User: DataTypes.STRING,
    Type: DataTypes.STRING,
    IdLooked: DataTypes.STRING
  }, {});
  registroprof.associate = function(models) {
    // associations can be defined here
  };
  return registroprof;
};