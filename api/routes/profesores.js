var express = require("express");
var router = express.Router();
var models = require("../models");
const jwt = require("jsonwebtoken");
const secreto = "llavesecreta";



router.get("/",verificarToken,(req, res) => {
  const paginaActual = parseInt(req.query.paginaActual);
  const cantidadAMostrar = parseInt(req.query.cantidadAMostrar);
  
  jwt.verify(req.token,secreto,(error, authData)=>{
    if(error){
      res.status(400).send("No tiene persmiso de observar los profesores..!!!!");
    }
    else{
      console.log("Esto es un mensaje para ver en consola");
      var contenidoToken = jwt.decode(req.token, secreto);
        var id = contenidoToken.user.id;
        var user = contenidoToken.user.mail;
        var type = 'GetAll';
        var looked = 'All';
      models.profesor
        .findAll({
          attributes: ["id", "nombre"],
          include:[{as:'Materia-Relacionada', model:models.materia, attributes:["id","nombre"]}],
          offset: (paginaActual - 1) * cantidadAMostrar,
          limit: cantidadAMostrar 
        })
        .then(profesores => res.send(profesores),
          registrarMovimientoProf(id, user, type, looked))
        .catch(() => res.sendStatus(500));
    }
    
  });
});

router.post("/",verificarToken,(req, res) => {
  jwt.verify(req.token,secreto,(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      var contenidoToken = jwt.decode(req.token, secreto);
        var id = contenidoToken.user.id;
        var user = contenidoToken.user.mail;
        var type = 'Post';
        var looked = ''
      models.profesor
        .create({ nombre: req.body.nombre , id_materia: req.body.id_materia})
        .then(profesor => {
            res.status(201).send({ id: profesor.id });
            looked = profesor.id;
            registrarMovimientoProf(id, user, type, looked);
        })
        .catch(error => {
          if (error == "SequelizeUniqueConstraintError: Validation error") {
            res.status(400).send('Bad request: No se pudo realizar la carga.')
          }
          else {
            console.log(`Error al intentar insertar en la base de datos: ${error}`)
            res.sendStatus(500)
          }
        });
    }
  });
});

const findprofesor = (id, { onSuccess, onNotFound, onError }) => {
  models.profesor
    .findOne({
      attributes: ["id", "nombre"],
      include:[{as:'Materia-Relacionada', model:models.materia, attributes:["id","nombre"]}],
      where: { id }
    })
    .then(profesor => (profesor ? onSuccess(profesor) : onNotFound()))
    .catch(() => onError());
};



router.get("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,secreto,(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      var contenidoToken = jwt.decode(req.token, secreto);
        var id = contenidoToken.user.id;
        var user = contenidoToken.user.mail;
        var type = 'GetId';
        var looked = '';
      findprofesor(req.params.id, {
        onSuccess: profesor => {res.send(profesor); looked = profesor.id, registrarMovimientoProf(id, user, type, looked)}, 
        onNotFound: () => res.send("El id solicitado no existe."),//res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});

router.put("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,secreto,(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      var contenidoToken = jwt.decode(req.token, secreto);
        var id = contenidoToken.user.id;
        var user = contenidoToken.user.mail;
        var type = 'Put';
        var looked = req.params.id;
      const onSuccess = profesor =>
        profesor
          .update({ nombre: req.body.nombre}, { fields: ["nombre"] })
          .then(() => res.sendStatus(200), registrarMovimientoProf(id, user, type, looked))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: existe otro profesor con el mismo nombre')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
      findprofesor(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});

router.delete("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,secreto,(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      var contenidoToken = jwt.decode(req.token, secreto);
        var id = contenidoToken.user.id;
        var user = contenidoToken.user.mail;
        var type = 'Del';
        var looked = req.params.id;
      const onSuccess = profesor =>
      profesor
        .destroy()
        .then(() => res.sendStatus(200), registrarMovimientoProf(id, user, type, looked))
        .catch(() => res.sendStatus(500));
      findprofesor(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});


function registrarMovimientoProf(id, user, type, looked){
  models.registroprof
  .create({ IdUser: id , User: user, Type: type, IdLooked: looked})
  .then(()=> console.log("Movimiento registrado."))
  .catch(() => console.log("No se pudo generar el registro."))
};


function verificarToken(req, res, run){
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined' ){
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    run();
  }
  else{
    res.sendStatus(403);
  }
}

module.exports = router;
