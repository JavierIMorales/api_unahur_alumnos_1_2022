var express = require("express");
var router = express.Router();
var models = require("../models");
var jwt = require("jsonwebtoken");
const { token } = require("morgan");



router.get("/",verificarToken,(req, res) => {
  const paginaActual = parseInt(req.query.paginaActual);
  const cantidadAMostrar = parseInt(req.query.cantidadAMostrar);
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("No tiene permiso de observar las carreras..!!");
    }
    else{
      console.log("Esto es un mensaje para ver en consola");
      models.carrera
      .findAll({
        attributes: ["id", "nombre"],
        include:[{as:'Materias-Relacionadas', model:models.materia, attributes:["nombre"]}],
        //offset:(paginaActual*cantidadAMostrar), cantidadAMostrar,
        offset: (paginaActual - 1) * cantidadAMostrar,
        limit: cantidadAMostrar
      })
      .then(carreras => res.send(carreras))
      .catch(() => res.sendStatus(500));
    }
  });
});


//esqueleto de la verificación del token para utilizar con los métodos.

// router.get("/",verificarToken,(req, res) => {
//   jwt.verify(req.token,'llavesecreta',(error, authData)=>{
//     if(error){
//       res.status(400).send("POR AQUI NO ..!!");
//     }
//     else{
      
//     }
//   });
// });







router.post("/",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      models.carrera
      .create({ nombre: req.body.nombre })
      .then(carrera => res.status(201).send({ id: carrera.id }))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
        }
        else {
          console.log(`Error al intentar insertar en la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    }
  });
});
// router.post("/", (req, res) => {
//   models.carrera
//     .create({ nombre: req.body.nombre })
//     .then(carrera => res.status(201).send({ id: carrera.id }))
//     .catch(error => {
//       if (error == "SequelizeUniqueConstraintError: Validation error") {
//         res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
//       }
//       else {
//         console.log(`Error al intentar insertar en la base de datos: ${error}`)
//         res.sendStatus(500)
//       }
//     });
// });




const findCarrera = (id, { onSuccess, onNotFound, onError }) => {
  models.carrera
    .findOne({
      attributes: ["id", "nombre"],
      where: { id },
      include:[{as:'Materias-Relacionadas', model:models.materia, attributes:["nombre"]}]
    })
    .then(carrera => (carrera ? onSuccess(carrera) : onNotFound()))
    .catch(() => onError());
};


router.get("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      findCarrera(req.params.id, {
        onSuccess: carrera => res.send(carrera),
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});
// router.get("/:id", (req, res) => {
//   findCarrera(req.params.id, {
//     onSuccess: carrera => res.send(carrera),
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//   });
// });



router.put("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      const onSuccess = carrera =>
        carrera
          .update({ nombre: req.body.nombre }, { fields: ["nombre"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findCarrera(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});
// router.put("/:id", (req, res) => {
//   const onSuccess = carrera =>
//     carrera
//       .update({ nombre: req.body.nombre }, { fields: ["nombre"] })
//       .then(() => res.sendStatus(200))
//       .catch(error => {
//         if (error == "SequelizeUniqueConstraintError: Validation error") {
//           res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
//         }
//         else {
//           console.log(`Error al intentar actualizar la base de datos: ${error}`)
//           res.sendStatus(500)
//         }
//       });
//     findCarrera(req.params.id, {
//     onSuccess,
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//   });
// });




router.delete("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      const onSuccess = carrera =>
        carrera
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
        findCarrera(req.params.id, {
          onSuccess,
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
    }
  });
});
// router.delete("/:id", (req, res) => {
//   const onSuccess = carrera =>
//     carrera
//       .destroy()
//       .then(() => res.sendStatus(200))
//       .catch(() => res.sendStatus(500));
//   findCarrera(req.params.id, {
//     onSuccess,
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//   });
// });


function verificarToken(req, res, run){
    const bearerHeader = req.headers['authorization'];

    if(typeof bearerHeader !== 'undefined' ){
      const bearerToken = bearerHeader.split(" ")[1];
      req.token = bearerToken;
      run();
    }
    else{
      res.sendStatus(403);
    }
}
module.exports = router;
