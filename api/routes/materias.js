var express = require("express");
var router = express.Router();
var models = require("../models");
//const materia = require("../models/materia");
var jwt = require("jsonwebtoken");




router.get("/",verificarToken,(req, res) => {
  const paginaActual = parseInt(req.query.paginaActual);
  const cantidadAMostrar = parseInt(req.query.cantidadAMostrar);
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("No tiene persmiso de observar las materias..!!");
    }
    else{
      console.log("Esto es un mensaje para ver en consola");
      models.materia
        .findAll({
          attributes: ["id", "nombre"],
          include:[{as:'Profesores-Relacionados', model:models.profesor, attributes:["id","nombre"]}],
          //agregamos para el paginado
          //offset:(paginaActual*cantidadAMostrar), cantidadAMostrar,
          offset: (paginaActual - 1) * cantidadAMostrar,
          limit: cantidadAMostrar  
          //hasta aca agregamos para el paginado
        })
        .then(materias => res.send(materias))
        .catch(() => res.sendStatus(500));
    }
  });
});

// router.get("/", (req, res) => {
//   console.log("Esto es un mensaje para ver en consola");
//   models.materia
//     .findAll({
//       attributes: ["id", "nombre"],
//       include:[{as:'Profesores-Relacionados', model:models.profesor, attributes:["id","nombre"]}]
//     })
//     .then(materias => res.send(materias))
//     .catch(() => res.sendStatus(500));
// });




router.post("/",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      models.materia
        .create({ nombre: req.body.nombre , id_carrera: req.body.id_carrera})
        .then(materia => res.status(201).send({ id: materia.id }))
        .catch(error => {
          if (error == "SequelizeUniqueConstraintError: Validation error") {
            res.status(400).send('Bad request: existe otra materia con el mismo nombre')
          }
          else {
            console.log(`Error al intentar insertar en la base de datos: ${error}`)
            res.sendStatus(500)
          }
        });
    }
  });
});
// router.post("/", (req, res) => {
//   models.materia
//     .create({ nombre: req.body.nombre , id_carrera: req.body.id_carrera})
//     .then(materia => res.status(201).send({ id: materia.id }))
//     .catch(error => {
//       if (error == "SequelizeUniqueConstraintError: Validation error") {
//         res.status(400).send('Bad request: existe otra materia con el mismo nombre')
//       }
//       else {
//         console.log(`Error al intentar insertar en la base de datos: ${error}`)
//         res.sendStatus(500)
//       }
//     });
// });

const findMateria = (id, { onSuccess, onNotFound, onError }) => {
  models.materia
    .findOne({
      attributes: ["id", "nombre"],
      where: { id },
      include:[{as:'Profesores-Relacionados', model:models.profesor, attributes:["id","nombre"]}]
    })
    .then(materia => (materia ? onSuccess(materia) : onNotFound()))
    .catch(() => onError());
};


router.get("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      findMateria(req.params.id, {
        onSuccess: materia => res.send(materia),
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
    }
  });
});
// router.get("/:id", (req, res) => {
//     findMateria(req.params.id, {
//     onSuccess: materia => res.send(materia),
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//   });
// });



router.put("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      const onSuccess = materia =>
        materia
          .update({ nombre: req.body.nombre}, { fields: ["nombre"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: existe otra materia con el mismo nombre')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findMateria(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
        });
    }
  });
});

// router.put("/:id", (req, res) => {
//   const onSuccess = materia =>
//     materia
//       .update({ nombre: req.body.nombre}, { fields: ["nombre"] })
//       .then(() => res.sendStatus(200))
//       .catch(error => {
//         if (error == "SequelizeUniqueConstraintError: Validation error") {
//           res.status(400).send('Bad request: existe otra materia con el mismo nombre')
//         }
//         else {
//           console.log(`Error al intentar actualizar la base de datos: ${error}`)
//           res.sendStatus(500)
//         }
//       });
//     findMateria(req.params.id, {
//     onSuccess,
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//     });
// });




router.delete("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      const onSuccess = materia =>
        materia
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
          findMateria(req.params.id, {
            onSuccess,
            onNotFound: () => res.sendStatus(404),
            onError: () => res.sendStatus(500)
          });
    }
  });
});
// router.delete("/:id", (req, res) => {
//   const onSuccess = materia =>
//     materia
//       .destroy()
//       .then(() => res.sendStatus(200))
//       .catch(() => res.sendStatus(500));
//   findMateria(req.params.id, {
//     onSuccess,
//     onNotFound: () => res.sendStatus(404),
//     onError: () => res.sendStatus(500)
//   });
// });



function verificarToken(req, res, run){
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined' ){
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    run();
  }
  else{
    res.sendStatus(403);
  }
}

module.exports = router;
